import Vue from "vue";
import Router from "vue-router";
import homepage from "../components/homepage.vue";
import register from '../components/register.vue';
import product from '../components/product.vue';
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "homepage",
      component: homepage
    },
    {
      path: "/register",
      name: "register",
      component: register
    },
    {
      path: "/product",
      name: "product",
      component: product
    }
  ]
});
